/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import sakda.oxproject.Ox;

/**
 *
 * @author PC Sakda
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticlePlayerOWinCol1() {
        char table[][] = {{'O', '-', '-'},
                                {'O', '-', '-'},
                                {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticlePlayerOWinCol2() {
        char table[][] = {{'-', 'O', '-'},
                                {'-', 'O', '-'},
                                {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticlePlayerOWinCol3() {
        char table[][] = {{'-', '-', 'O'},
                                {'-', '-', 'O'},
                                {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckHorizontalPlayerOWinRow1() {
        char table[][] = {{'O', 'O', 'O'},
                                {'-', '-', '-'},
                                {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerOWinRow2() {
        char table[][] = {{'-', '-', '-'},
                                {'O', 'O', 'O'},
                                {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerOWinRow3() {
        char table[][] = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckOWinX1() {
        char table[][] = {{'O', '-', '-'},
                                {'-', 'O', '-'},
                                {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, Ox.checkX1(table, currentPlayer));
    }
    @Test
    public void testCheckOWinX2() {
        char table[][] = {{'-', '-', 'O'},
                                {'-', 'O', '-'},
                                {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, Ox.checkX2(table, currentPlayer));
    }
    @Test
    public void testCheckVerticlePlayerXWinCol1() {
        char table[][] = {{'X', '-', '-'},
                                {'X', '-', '-'},
                                {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticlePlayerXWinCol2() {
        char table[][] = {{'-', 'X', '-'},
                                {'-', 'X', '-'},
                                {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticlePlayerXWinCol3() {
        char table[][] = {{'-', '-', 'X'},
                                {'-', '-', 'X'},
                                {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;

        assertEquals(true, Ox.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckHorizontalPlayerXWinRow1() {
        char table[][] = {{'X', 'X', 'X'},
                                {'-', '-', '-'},
                                {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerXWinRow2() {
        char table[][] = {{'-', '-', '-'},
                                {'X', 'X', 'X'},
                                {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckHorizontalPlayerXWinRow3() {
        char table[][] = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;

        assertEquals(true, Ox.checkHorizontal(table, currentPlayer, row));
    }
    @Test
    public void testCheckXWinX1() {
        char table[][] = {{'X', '-', '-'},
                                {'-', 'X', '-'},
                                {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, Ox.checkX1(table, currentPlayer));
    }
    @Test
    public void testCheckXWinX2() {
        char table[][] = {{'-', '-', 'X'},
                                {'-', 'X', '-'},
                                {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, Ox.checkX2(table, currentPlayer));
    }
    @Test
    public void testCheckDraw() {
       int count =9; 
        assertEquals(true,Ox.checkDraw(count)); 
        
    }
}
