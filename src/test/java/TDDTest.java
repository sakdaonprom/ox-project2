/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author PC Sakda
 */
public class TDDTest {
    
    public TDDTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testAdd_1_2is3() {
         assertEquals(3, Example.add(1,2));
     }
     @Test
     public void testAdd_3_4is7() {
         assertEquals(7, Example.add(3,4));
     }
     @Test
     public void testAdd_20_22is42() {
         assertEquals(42, Example.add(20,22));
     }
     // p:paper, s:scissors, h:hammer
     // chup(char player1, char  player2) -> "p1" , "p2","Draw"
     // Draw
     @Test
     public void testChop_p1_p_p2_p_is_draw() {
         assertEquals("draw", Example.chup('p','p'));
     }
     @Test
     public void testChop_p1_s_p2_s_is_draw() {
         assertEquals("draw", Example.chup('s','s'));
     }
     @Test
     public void testChop_p1_h_p2_h_is_draw() {
         assertEquals("draw", Example.chup('h','h'));
     }
     //Player1 Win
     @Test
     public void testChop_p1_s_p2_p_is_p1() {
         assertEquals("p1", Example.chup('s','p'));
     }
     @Test
     public void testChop_p1_h_p2_s_is_p1() {
         assertEquals("p1", Example.chup('h','s'));
     }
     @Test
     public void testChop_p1_p_p2_h_is_p1() {
         assertEquals("p1", Example.chup('p','h'));
     }
     //Player2 Win
     @Test
     public void testChop_p1_h_p2_p_is_p2() {
         assertEquals("p2", Example.chup('h','p'));
     }
     @Test
     public void testChop_p1_s_p2_h_is_p2() {
         assertEquals("p2", Example.chup('s','h'));
     }
     @Test
     public void testChop_p1_p_p2_s_is_p2() {
         assertEquals("p2", Example.chup('p','s'));
     }
}
